---
sidebar_position: 1
---

# Full graph dump

You can download the full OpenAIRE Research Graph Dump as well as its schema from the following links: 

 Dataset: https://doi.org/10.5281/zenodo.3516917
    
 Schema: https://doi.org/10.5281/zenodo.4238938

The schema used to dump this dataset mirrors  the one described in the [Data Model](../data-model).
This dataset is licensed under a Creative Commons Attribution 4.0 International License.
It is composed of several files so that you can download the parts you are interested into. The files are named after the entity they store (i.e. publication, dataset). Each file is at most 10GB and it is 
a tar archive containing gz files, each with one json per line.

## How to acknowledge this work

Open Science services are open and transparent and survive thanks to your active support and to the visibility and reward they gather. If you use one of the [OpenAIRE Research Graph dumps](https://doi.org/10.5281/zenodo.3516917) for your research, please provide a proper citation following the recommendation that you find on the dump's Zenodo page or as provided below. 

:::note How to cite

Manghi P., Atzori C., Bardi A., Baglioni M., Schirrwagen J., Dimitropoulos H., La Bruzzo S., Foufoulas I., Mannocci A., Horst M., Czerniak A., Kiatropoulou K., Kokogiannaki A., De Bonis M., Artini M., Ottonello E., Lempesis A., Ioannidis A., Manola N., Principe P. (2022). "OpenAIRE Research Graph Dump", *Dataset*, Zenodo. [doi:10.5281/zenodo.3516917](https://doi.org/10.5281/zenodo.3516917) ([BibTex](/bibtex/OpenAIRE_Research_Graph_dump.bib))
:::

Please also consider citing [other relevant research products](/publications#relevant-research-products) that can be of interest.

Also consider adding one of the following badges to your service with the appropriate link to [our website](https://graph.openaire.eu); click on the badges below to download the respective badge image files.


<div className="row">
    <div className="col col--4 left-badge">
        <a target="_blank" href={require('../assets/badges/openaire-badge-1.zip').default} download>
            <img loading="lazy" alt="Openaire badge" src={require('../assets/badges/openaire-badge-1.png').default} className="img_node_modules-@docusaurus-theme-classic-lib-theme-MDXComponents-Img-styles-module pagination-nav__link" style={{ paddingTop: '1.2em', paddingBottom: '1.2em'}} title="Click to download"/>
        </a>  
    </div>
    <div className="col col--4 mid-badge">
        <a target="_blank" href={require('../assets/badges/openaire-badge-2.zip').default} download>
            <img loading="lazy" alt="Openaire badge" src={require('../assets/badges/openaire-badge-2.png').default} className="img_node_modules-@docusaurus-theme-classic-lib-theme-MDXComponents-Img-styles-module pagination-nav__link  dark-badge" style={{ paddingTop: '1.2em', paddingBottom: '1.2em'}} title="Click to download"/>
        </a>  
    </div>
    <div className="col col--4 right-badge">
        <a target="_blank" href={require('../assets/badges/openaire-badge-3.zip').default} download>
            <img loading="lazy" alt="Openaire badge" src={require('../assets/badges/openaire-badge-3.png').default} className="img_node_modules-@docusaurus-theme-classic-lib-theme-MDXComponents-Img-styles-module pagination-nav__link" style={{ paddingTop: '1.2em', paddingBottom: '1.2em'}} title="Click to download"/>
        </a>
    </div>
</div>
